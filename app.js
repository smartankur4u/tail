const readLastLines = require('./utils/read-last-lines')

const fs = require('fs')

const noOfLines = process.argv[2]

// initial data
readLastLines.read('test.txt', noOfLines)
  .then((lines) => console.log(lines))

fs.watchFile(
  'test.txt',
  { persistent: true, interval: 0 },
  (curr, prev) => {
    console.log(`File changed at: ${curr.mtime}, last ${noOfLines} lines are::`)
    readLastLines.read('test.txt', noOfLines)
      .then((lines) => console.log(lines))
  }
)

