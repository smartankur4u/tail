# tail
Implementation of tail command of shell


## Install dependencies

```bash
npm install
```


## Usage

```bash
npm start N

```
Here, N is the number of lines to print.
